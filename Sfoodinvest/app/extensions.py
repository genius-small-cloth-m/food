from flask_sqlalchemy import SQLAlchemy

# 创建对象
db = SQLAlchemy()

# 初始化
def config_extensions(app):

    db.init_app(app)


