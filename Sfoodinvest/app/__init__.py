from flask import Flask, render_template
from app.config import config
from app.extensions import config_extensions
from app.views import config_blueprint


# 将创建app的动作封装成一个函数
def create_app(config_name):
    # 创建app实例对象
    app = Flask(__name__)
    # 加载配置
    app.config.from_object(config.get(config_name) or 'default')
    # 执行额外的初始化
    config.get(config_name).init_app(app)

    # 加载扩展
    config_extensions(app)

    # 配置蓝本
    config_blueprint(app)

    # 返回app实例对象
    return app