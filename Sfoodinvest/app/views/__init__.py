from .show import show

# DEFAULT_BLUEPRINT = (
#     (show, '/show')
#
# )

# 封装配置蓝本的函数
def config_blueprint(app):
    # 循环读取元组中的蓝本
    app.register_blueprint(show, url_prefix='/show')