import os
from app.models.tabs import Tabs
from flask import Blueprint, render_template, current_app, redirect, url_for, flash, request, render_template_string
from itsdangerous import TimedJSONWebSignatureSerializer as Serializer
from app.extensions import db
#basedir = os.path.abspath(os.path.dirname(__file__))
# 创建蓝本对象
show = Blueprint('show', __name__)

# 视图函数
@show.route('/login',methods=['GET', 'POST'])
def login():
    result = Tabs.query.all()
    # for i in result:
    #     print(i.label)
    #
    # tabs = Tabs()
    # tabs.url = "dsafjsajfs"
    # tabs.label = "剩饭"
    # db.session.add(tabs)
    # db.session.commit()
    return render_template('show.html',result=result)

# 视图函数
@show.route('/upload',methods=['GET', 'POST'])
def Toupload():
    return render_template('upload.html')


@show.route('/up_photo', methods=['post'])
def up_photo():
    img = request.files.get('photo')
    username = request.form.get("name")
    path = "E:\PycharmProject\Sfoodinvest\\app\static\photo\\"
    file_path = path + img.filename
    print(file_path)
    img.save(file_path)
    print('上传头像成功，上传的用户是：' + username)
    return render_template('show.html')
