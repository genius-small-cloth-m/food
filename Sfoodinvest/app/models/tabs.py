from app.extensions import db


class Tabs(db.Model):
    __tablename__ = 'tabs'
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    #图片保存路径
    url = db.Column(db.String,nullable=False, default="")
    #标签类型
    label = db.Column(db.String, nullable=False, default="")
